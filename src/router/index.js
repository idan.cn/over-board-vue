import Vue from 'vue'
import Router from 'vue-router'
import Drawings from '@/components/Drawings'
import DrawingDetails from '@/components/DrawingDetails'
import Board from '@/components/Board'
import VueResource from 'vue-resource'
import VueMdc from 'vue-mdc'
import VueMoment from 'vue-moment'
import 'vue-mdc/dist/vue-mdc.css'

Vue.use(VueMdc)
Vue.use(Router)
Vue.use(VueResource)
Vue.use(VueMoment)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'drawings',
			component: Drawings
		},
		{
			path: '/new',
			name: 'board',
			component: Board
		},
		{
			path: '/:id',
			name: 'drawing',
			component: DrawingDetails
		}
	]
})
