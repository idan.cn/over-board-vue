# OVERBOARD

(H)OVERBOARD is a simple drawing app.

## Where can i see it in action?

Overboard is hosted on heroku. You can see it live here: https://us-overboard-vue.herokuapp.com.  
Please note that loading it for the first time might take a bit longer than normal, this is because of heroku free dynos policy, it means that the app is going to "sleep" after a while of inactivity and waking it up may take a bit longer.

## The stack

Overboard is powered with node.js & express on the back end, MySQL as a persistency layer and vue.js in the front end.

Why MySQL? It is a solid choice for simple save and fetch drawings, there is no complex queries (e.g data aggregations or text search) has to be done, so it should provide good performance. it's also mature, well maintained technology and widely used.

## Trade-offs
Or, what have i would done differently if i had more time?

- Use [Vuex](https://vuex.vuejs.org/en/intro.html) as a centralized state manager. I would create a [module](https://vuex.vuejs.org/en/modules.html) for board related state, this would make the communication between components like the color picker and board canvas a bit easier.
- Use CSS Preprocessor like Sass or Less, nesting CSS rules make CSS more readable and the ability to define variables allows re-usable CSS.
- Use cheap data storage like S3 to store the drawings as bitmaps instead of data uris in the DB - mainly because scaling will be cheaper, DB space is considered more expensive than storing the images in the file system or in 3rd party service like S3.
- Add unit tests where needed, a good candidate for a unit test can be the drawStep function in the DrawingDetails component, this will allow making changes in that function with more confidence.
- Avoid making cross origin requests - currently, an ajax request target is in the form of "https://us-over-board-server.herokuapp.com/api/\*", a better practice would be using "api/\*" and lettings the host be relative to the environment the web app currently runs on. this can be achieved by creating a proxy on the server which serves the app static files. It will allow the removal of the middleware which allows incoming request from every source, making the api layer more secure.
- Save drawings in a more efficient way - currently, each drawing is represented and saved as an array of arrays (segments). Each segment (from mouse down to mouse up) is saved as an array of points, each point has X & Y coordinates, brush color and brush size. Saving color and size per point is unnecessary and should be saved per segment, a segment, by definition should have one color and size of brush. This will reduce drawing size in the DB.
- Improve error handling on the front end. failing to save or fetch drawings should be handled with an appropriate message to the user.

## Known issues

Saving an empty drawing generates an error.